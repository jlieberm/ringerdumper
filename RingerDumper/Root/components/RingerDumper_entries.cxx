

#include "RingerDumper/RingerDumperAlg.h"
#include "RingerDumper/ForwardRingerDumperAlg.h"
#include "RingerDumper/PhotonRingerDumperAlg.h"

DECLARE_COMPONENT( RingerDumperAlg )
DECLARE_COMPONENT( ForwardRingerDumperAlg )
DECLARE_COMPONENT( PhotonRingerDumperAlg )