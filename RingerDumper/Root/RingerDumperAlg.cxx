// PhotonTruthMatchingTool includes
#include "RingerDumper/RingerDumperAlg.h"
#include <iostream>
#include <vector>

RingerDumperAlg::RingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ), m_ntsvc("THistSvc/THistSvc", name){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


RingerDumperAlg::~RingerDumperAlg() {
 
}


StatusCode RingerDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  m_Tree = new TTree("events", "events");
    bookBranches(m_Tree);
    if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [events]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

void RingerDumperAlg::bookBranches(TTree *tree){
  tree->Branch("avgmu", &m_pileup);
  if (m_dumpCells){
    tree->Branch("cells_eta", &m_cells_eta);
    tree->Branch("cells_phi", &m_cells_phi);
    tree->Branch("cells_et", &m_cells_et);
    tree->Branch("cells_sampling", &m_cells_sampling);
    tree->Branch("cells_size", &m_cells_size);
    tree->Branch("std_rings_sum", &m_rings_sum);    
    // truth
    tree->Branch("cellsTruth_eta", &m_Truthcells_eta);
    tree->Branch("cellsTruth_phi", &m_Truthcells_phi);
    tree->Branch("cellsTruth_et", &m_Truthcells_et);
    tree->Branch("cellsTruth_sampling", &m_Truthcells_sampling);
    tree->Branch("cellsTruth_size", &m_Truthcells_size);
    tree->Branch("std_Truthrings_sum", &m_Truthrings_sum);
  }
  tree->Branch("online_rings", &m_rings);
  tree->Branch("cluster_et", &m_clusterEnergy);
  tree->Branch("cluster_eta", &m_clusterEta);
  tree->Branch("cluster_phi", &m_clusterPhi);
  tree->Branch("cluster_e237", &m_clustere237);
  tree->Branch("cluster_e277", &m_clustere277);
  tree->Branch("cluster_fracs1", &m_clusterfracs1);
  tree->Branch("cluster_weta2", & m_clusterweta2);
  tree->Branch("cluster_wstot", & m_clusterwstot);
  tree->Branch("cluster_ehad1", & m_clusterehad1);
  // truth 
  tree->Branch("online_Truthrings", &m_Truthrings);
  tree->Branch("cluster_Truthet", &m_TruthclusterEnergy);
  tree->Branch("cluster_Trutheta", &m_TruthclusterEta);
  tree->Branch("cluster_Truthphi", &m_TruthclusterPhi);
  tree->Branch("cluster_Truthe237", &m_Truthclustere237);
  tree->Branch("cluster_Truthe277", &m_Truthclustere277);
  tree->Branch("cluster_Truthfracs1", &m_Truthclusterfracs1);
  tree->Branch("cluster_Truthweta2", & m_Truthclusterweta2);
  tree->Branch("cluster_Truthwstot", & m_Truthclusterwstot);
  tree->Branch("cluster_Truthehad1", & m_Truthclusterehad1);

  tree->Branch("offline_rings", &m_offlineRings);
  tree->Branch("el_energy", &el_energy);
  tree->Branch("el_eta", &el_eta);
  tree->Branch("el_phi", &el_phi);
  tree->Branch("el_f1",&el_f1);
  tree->Branch("el_f3",&el_f3);
  tree->Branch("el_eratio",&el_eratio);
  tree->Branch("el_weta1",&el_weta1);
  tree->Branch("el_weta2",&el_weta2);
  tree->Branch("el_fracs1",&el_fracs1);
  tree->Branch("el_wtots1",&el_wtots1);
  tree->Branch("el_deltae",&el_deltae);
  tree->Branch("el_e2777",&el_e277);
  tree->Branch("el_reta",&el_reta);
  tree->Branch("el_rphim",&el_rphi);
  tree->Branch("el_rhad",&el_rhad);
  tree->Branch("el_rhad1",&el_rhad1);

  tree->Branch("el_Truth_pt", &el_Truth_pt);
  tree->Branch("el_Truth_eta", &el_Truth_eta);
  tree->Branch("el_Truth_phi", &el_Truth_phi);
  tree->Branch("el_Truth_f1",&el_Truth_f1);
  tree->Branch("el_Truth_f3",&el_Truth_f3);
  tree->Branch("el_Truth_eratio",&el_Truth_eratio);
  tree->Branch("el_Truth_weta1",&el_Truth_weta1);
  tree->Branch("el_Truth_weta2",&el_Truth_weta2);
  tree->Branch("el_Truth_fracs1",&el_Truth_fracs1);
  tree->Branch("el_Truth_wtots1",&el_Truth_wtots1);
  tree->Branch("el_Truth_deltae",&el_Truth_deltae);
  tree->Branch("el_Truth_e277",&el_Truth_e277);
  tree->Branch("el_Truth_reta",&el_Truth_reta);
  tree->Branch("el_Truth_rphi",&el_Truth_rphi);
  tree->Branch("el_Truth_rhad",&el_Truth_rhad);
  tree->Branch("el_Truth_rhad1",&el_Truth_rhad1);


  if (m_doMC){
    tree->Branch("mc_et", &mc_et);
    tree->Branch("mc_eta", &mc_eta);
    tree->Branch("mc_phi", &mc_phi);
    tree->Branch("mc_origin", &mc_origin);
    tree->Branch("mc_type", &mc_type);

    tree->Branch("mc_match_et", &mc_match_et);
    tree->Branch("mc_match_eta", &mc_match_eta);
    tree->Branch("mc_match_phi", &mc_match_phi);
    tree->Branch("mc_match_origin", &mc_match_origin);
    tree->Branch("mc_match_type", &mc_match_type);
  }
}

StatusCode RingerDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  if (!collectInfo()) ATH_MSG_DEBUG("Unable to collect info. Please check input file");
  m_Tree->Fill();
  return StatusCode::SUCCESS;
}

StatusCode RingerDumperAlg::collectInfo(){
  clear();
  if(!collectEventInfo())   ATH_MSG_DEBUG("Event information cannot be collected!");
  if(!collectRingsInfo())   ATH_MSG_DEBUG("Ringer information cannot be collected!");
  if(!collectOfflineInfo()) ATH_MSG_DEBUG("Offline information cannot be collected!");
  return StatusCode::SUCCESS;
}

void RingerDumperAlg::clear(){
  m_pileup = -999;
  if(m_dumpCells){
    m_cells_eta->clear();
    m_cells_phi->clear();
    m_cells_et->clear();
    m_cells_sampling->clear();
    m_cells_size->clear();
    m_rings_sum->clear();

    m_Truthcells_eta->clear();
    m_Truthcells_phi->clear();
    m_Truthcells_et->clear();
    m_Truthcells_sampling->clear();
    m_Truthcells_size->clear();
  }
  m_rings->clear();
  m_clusterEnergy->clear();
  m_clusterEta->clear();
  m_clusterPhi->clear();
  m_clustere237->clear();
  m_clustere277->clear();
  m_clusterfracs1->clear();
  m_clusterweta2->clear();
  m_clusterwstot->clear();
  m_clusterehad1->clear();
  m_offlineRings->clear();
// truth
  m_Truthrings->clear();
  m_TruthclusterEnergy->clear();
  m_TruthclusterEta->clear();
  m_TruthclusterPhi->clear();
  m_Truthclustere237->clear();
  m_Truthclustere277->clear();
  m_Truthclusterfracs1->clear();
  m_Truthclusterweta2->clear();
  m_Truthclusterwstot->clear();
  m_Truthclusterehad1->clear(); 
  m_Truthrings_sum->clear();

  el_energy->clear();
  el_eta->clear();
  el_phi->clear();
  el_f1->clear();
  el_f3->clear();
  el_eratio->clear();
  el_weta1->clear();
  el_weta2->clear();
  el_fracs1->clear();
  el_wtots1->clear();
  el_deltae->clear();
  el_e277->clear();
  el_reta->clear();
  el_rphi->clear();
  el_rhad->clear();
  el_rhad1->clear();

// offline truth
  el_Truth_pt->clear();
  el_Truth_eta->clear();
  el_Truth_phi->clear();
  el_Truth_f1->clear();
  el_Truth_f3->clear();
  el_Truth_eratio->clear();
  el_Truth_weta1->clear();
  el_Truth_weta2->clear();
  el_Truth_fracs1->clear();
  el_Truth_wtots1->clear();
  el_Truth_e277->clear();
  el_Truth_reta->clear();
  el_Truth_rphi->clear();
  el_Truth_deltae->clear();
  el_Truth_rhad->clear();
  el_Truth_rhad1->clear();

  if(m_doMC){
    mc_et->clear();
    mc_eta->clear();
    mc_phi->clear();
    mc_origin->clear();
    mc_type->clear();

    mc_match_et->clear();
    mc_match_eta->clear();
    mc_match_phi->clear();
    mc_match_origin->clear();
    mc_match_type->clear();
  }  
}

StatusCode RingerDumperAlg::collectRingsInfo(){
  const xAOD::TrigRingerRingsContainer *ringerContainer = nullptr;
  const xAOD::RingSetContainer *el_ring_container = nullptr;
  ATH_CHECK( evtStore()->retrieve(ringerContainer,"HLT_FastCaloRinger"));
  ATH_CHECK(evtStore()->retrieve(el_ring_container, "ElectronRingSets"));
  ATH_MSG_DEBUG("Rings Container Retrieved! That's good!");


  ATH_MSG_INFO("Collect Rings: --------- Looping over Ringer Container");

  for (const xAOD::TrigRingerRings_v2 *ringer : *ringerContainer){

    ATH_MSG_DEBUG("Ringer --- eta: " << ringer->emCluster()->eta() << " phi: " << ringer->emCluster()->phi() << " ET: " << ringer->emCluster()->et());

    if(m_dumpCells){
      m_cells_eta->push_back(ringer->auxdata<std::vector<float>>("cells_eta"));
      m_cells_phi->push_back(ringer->auxdata<std::vector<float>>("cells_phi"));
      m_cells_et->push_back(ringer->auxdata<std::vector<float>>("cells_et"));
      m_cells_sampling->push_back(ringer->auxdata<std::vector<int>>("cells_sampling"));
      m_cells_size->push_back(ringer->auxdata<std::vector<int>>("cells_size"));
      m_rings_sum->push_back(ringer->auxdata<std::vector<double>>("rings_sum"));
    }
    if (m_doQuarters){
      std::vector < float>  cells_eta = ringer->auxdata<std::vector<float>>("cells_eta");
      std::vector < float>  cells_phi = ringer->auxdata<std::vector<float>>("cells_phi");
      std::vector < float>  cells_et = ringer->auxdata<std::vector<float>>("cells_et");
      std::vector < int>  cells_sampling = ringer->auxdata<std::vector<int>>("cells_sampling");


    }
    m_rings->push_back(ringer->rings());
    m_clusterEnergy->push_back(ringer->emCluster()->et());
    m_clusterEta->push_back(ringer->emCluster()->eta());
    m_clusterPhi->push_back(ringer->emCluster()->phi());

    m_clustere237->push_back(ringer->emCluster()->e237());
    m_clustere277->push_back(ringer->emCluster()->e277());
    m_clusterfracs1->push_back(ringer->emCluster()->fracs1());
    m_clusterweta2->push_back(ringer->emCluster()->weta2());
    m_clusterwstot->push_back(ringer->emCluster()->wstot());
    m_clusterehad1->push_back(ringer->emCluster()->ehad1());
    }

  ATH_MSG_DEBUG("Online Standard Ringer Information Collected! That's good!");
  std::vector<float> single_ring;
  for (const xAOD::RingSet_v1 *el_ring : *el_ring_container) {
    for (auto iter=el_ring->begin(); iter != el_ring->end(); iter++){
      single_ring.push_back(*iter);
    }
    if (single_ring.size() == 100){
      m_offlineRings->push_back(single_ring);
      single_ring.clear();
    }
  }

  ATH_MSG_DEBUG("Offline Standard Ringer Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

StatusCode RingerDumperAlg::collectEventInfo(){
  const xAOD::EventInfo* ei = nullptr;
  ATH_CHECK( evtStore()->retrieve(ei,"EventInfo"));
  ATH_MSG_DEBUG("EventInfo Container Retrieved! That's good!");
  m_pileup =  ei->actualInteractionsPerCrossing();
  return StatusCode::SUCCESS;
}
StatusCode RingerDumperAlg::buildQuarters(std::vector<float> &cells_eta, std::vector<float> &cells_phi, std::vector<float> &cells_et, std::vector<int> &cells_sampling){
  std::vector<float> m_deltaEta = {0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.1};
  std::vector<float> m_deltaPhi = {0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170, 0.098174770424681, 0.098174770424681, 0.098174770424681};
  float eta_center = 0;
  float phi_center = 0;
  std::map<int, int> layers = {
    { 0, 0 },
    { 4, 0 },
    { 1, 1 },
    { 5, 1 },
    { 2, 2 },
    { 6, 2 },
    { 3, 3 },
    { 7, 3 },

};
  for (int cell=0; cell < cells_eta.size(); cell++){
    float k_deltaEta = m_deltaEta[layers[cells_sampling[cell]]];
    float k_deltaPhi = m_deltaPhi[layers[cells_sampling[cell]]];
    const double deltaEta = (cells_eta[cell] - eta_center)/k_deltaEta;
    const double deltaPhi = (cells_phi[cell] - phi_center)/k_deltaPhi;
  }
  return StatusCode::SUCCESS;
}
StatusCode RingerDumperAlg::collectOfflineInfo(){

  const xAOD::ElectronContainer *offlineElectrons = nullptr;
  const xAOD::TrigRingerRingsContainer *ringerContainer = nullptr;

  ATH_CHECK(evtStore()->retrieve(offlineElectrons, "Electrons"));
  ATH_MSG_DEBUG("Offline Electron Container Retrieved! That's good!");
  ATH_CHECK( evtStore()->retrieve(ringerContainer,"HLT_FastCaloRinger"));
  
  ATH_MSG_INFO("Ringer Container Retrieved! That's good!");
  ATH_MSG_INFO("looping over offline electrons");

  for (auto *electron : *offlineElectrons){
   
    ATH_MSG_DEBUG("eta: " << electron->eta() << " phi: " << electron->phi() << " e: " << electron->e());

    el_energy->push_back ( electron->e() );
    el_eta->push_back ( electron->eta() );
    el_phi->push_back ( electron->phi() );
  
    if (!collectOfflineSS(electron)) ATH_MSG_DEBUG("Impossible to collect offline SS variables");

    if (m_doMC){

      const xAOD::TruthParticle* mc = xAOD::TruthHelpers::getTruthParticle(*electron);
      if(mc){
        ATH_MSG_DEBUG("We have a electron truth particle ... ");

        mc_et->push_back(mc->pt());
        mc_eta->push_back(mc->eta());
        mc_phi->push_back(mc->phi());
        mc_origin->push_back(electron->auxdata<int>("truthOrigin"));
        mc_type->push_back(electron->auxdata<int>("truthType"));
        
        int count = 0; // getting only one cluster inside the container        
        for (auto *ringerTruth : *ringerContainer){
          
          // computing deltaR to match offline electrons between emClusters
          float deltaR=0.;
          float em_eta = ringerTruth->emCluster()->eta();
          float em_phi = ringerTruth->emCluster()->phi();
          
          float elec_eta = electron->eta();
          float elec_phi = electron->phi();

          deltaR = dR(em_eta,em_phi, elec_eta,elec_phi);
          
          if(deltaR > 0.07){
              continue;
          }else{

            ATH_MSG_INFO("ringer vs offline electron truth matching! deltaR = " << deltaR);
            ATH_MSG_DEBUG("em_eta: " << em_eta << " em_phi: " << em_phi << " em_et: " << ringerTruth->emCluster()->et());

            // store mc truth that have matching
            mc_match_et->push_back(mc->pt());
            mc_match_eta->push_back(mc->eta());
            mc_match_phi->push_back(mc->phi());
            mc_match_origin->push_back(electron->auxdata<int>("truthOrigin"));
            mc_match_type->push_back(electron->auxdata<int>("truthType"));

            if(m_dumpCells){
                m_Truthcells_eta->push_back(ringerTruth->auxdata<std::vector<float>>("cells_eta"));
                m_Truthcells_phi->push_back(ringerTruth->auxdata<std::vector<float>>("cells_phi"));
                m_Truthcells_et->push_back(ringerTruth->auxdata<std::vector<float>>("cells_et"));
                m_Truthcells_sampling->push_back(ringerTruth->auxdata<std::vector<int>>("cells_sampling"));
                m_Truthcells_size->push_back(ringerTruth->auxdata<std::vector<int>>("cells_size"));
                m_Truthrings_sum->push_back(ringerTruth->auxdata<std::vector<double>>("rings_sum"));
            }

            m_Truthrings->push_back(ringerTruth->rings());
            m_TruthclusterEnergy->push_back(ringerTruth->emCluster()->et());
            m_TruthclusterEta->push_back(ringerTruth->emCluster()->eta());
            m_TruthclusterPhi->push_back(ringerTruth->emCluster()->phi());
            m_Truthclustere237->push_back(ringerTruth->emCluster()->e237());
            m_Truthclustere277->push_back(ringerTruth->emCluster()->e277());
            m_Truthclusterfracs1->push_back(ringerTruth->emCluster()->fracs1());
            m_Truthclusterweta2->push_back(ringerTruth->emCluster()->weta2());
            m_Truthclusterwstot->push_back(ringerTruth->emCluster()->wstot());
            m_Truthclusterehad1->push_back(ringerTruth->emCluster()->ehad1());

            el_Truth_pt->push_back(electron->pt());
            el_Truth_eta->push_back(electron->eta());
            el_Truth_phi->push_back(electron->phi());
            el_Truth_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f1));
            el_Truth_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f3));
            el_Truth_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Eratio));
            el_Truth_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta1));
            el_Truth_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta2));
            el_Truth_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::fracs1));
            el_Truth_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::wtots1));
            el_Truth_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::e277));
            el_Truth_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Reta));
            el_Truth_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rphi));
            el_Truth_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::DeltaE));
            el_Truth_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad));
            el_Truth_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad1));

            count++;
            if (count > 0){
              break;
            }
          }    
        }        
      }
    }
  }
  
  ATH_MSG_DEBUG("Offline Electron Container Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

StatusCode  RingerDumperAlg::collectOfflineSS(const xAOD::Electron *electron){
  el_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f1));
  el_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f3));
  el_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Eratio));
  el_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta1));
  el_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta2));
  el_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::fracs1));
  el_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::wtots1));
  el_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::e277));
  el_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Reta));
  el_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rphi));
  el_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::DeltaE));
  el_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad));
  el_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad1));
   
  return StatusCode::SUCCESS;
}

StatusCode RingerDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}