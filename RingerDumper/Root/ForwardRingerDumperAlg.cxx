// PhotonTruthMatchingTool includes
#include "RingerDumper/ForwardRingerDumperAlg.h"




ForwardRingerDumperAlg::ForwardRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ), m_ntsvc("THistSvc/THistSvc", name){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("ForwardElectronIsEMSelectors", m_electronIsEMTool);

}


ForwardRingerDumperAlg::~ForwardRingerDumperAlg() {

}


StatusCode ForwardRingerDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  m_Tree = new TTree("events", "events");
    bookBranches(m_Tree);
    if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [events]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

void ForwardRingerDumperAlg::bookBranches(TTree *tree){
  tree->Branch("avgmu", &m_pileup);
  
  tree->Branch("cluster_et", &m_clusterEnergy);
  tree->Branch("cluster_eta", &m_clusterEta);
  tree->Branch("cluster_phi", &m_clusterPhi);
  tree->Branch("cluster_e237", &m_clustere237);
  tree->Branch("cluster_e277", &m_clustere277);
  tree->Branch("cluster_fracs1", &m_clusterfracs1);
  tree->Branch("cluster_weta2", & m_clusterweta2);
  tree->Branch("cluster_wstot", & m_clusterwstot);
  tree->Branch("cluster_ehad1", & m_clusterehad1);
  tree->Branch("el_ringsE", &m_offlineRings);
  tree->Branch("el_hasCalo", &m_hasCalo);
  tree->Branch("el_calo_e", &m_el_calo_e);
  tree->Branch("el_calo_eta", &m_el_calo_eta);
  tree->Branch("el_calo_phi", &m_el_calo_phi);
  tree->Branch("el_calo_et", &m_el_calo_et);
  tree->Branch("el_calo_etaBE2", &m_el_calo_etaBE2);
  tree->Branch("el_e", &el_energy);
  tree->Branch("el_et", &el_et);
  tree->Branch("el_eta", &el_eta);
  tree->Branch("el_phi", &el_phi);
  tree->Branch("el_f1",&el_f1);
  tree->Branch("el_f3",&el_f3);
  tree->Branch("el_Eratio",&el_eratio);
  tree->Branch("el_weta1",&el_weta1);
  tree->Branch("el_weta2",&el_weta2);
  tree->Branch("el_fracs1",&el_fracs1);
  tree->Branch("el_wtots1",&el_wtots1);
  tree->Branch("el_deltaE",&el_deltae);
  tree->Branch("el_e277",&el_e277);
  tree->Branch("el_Reta",&el_reta);
  tree->Branch("el_Rphi",&el_rphi);
  tree->Branch("el_Rhad",&el_rhad);
  tree->Branch("el_Rhad1",&el_rhad1);
  tree->Branch("el_tight",&m_el_tight);
  tree->Branch("el_medium",&m_el_medium);
  tree->Branch("el_loose",&m_el_loose);
  if (m_doMC){
    tree->Branch("mc_et", &mc_et);
    tree->Branch("mc_eta", &mc_eta);
    tree->Branch("mc_phi", &mc_phi);
    tree->Branch("mc_origin", &mc_origin);
    tree->Branch("mc_type", &mc_type);
  }
  if (m_dumpCells){
    tree->Branch("cells_eta", &m_cells_eta);
    tree->Branch("cells_phi", &m_cells_phi);
    tree->Branch("cells_et", &m_cells_et);
    tree->Branch("cells_sampling", &m_cells_sampling);
    tree->Branch("cells_ringNumber", &m_cells_ringNumber);
    tree->Branch("cells_seedEta", &m_cells_seedEta);
    tree->Branch("cells_seedPhi", &m_cells_seedPhi);
  }
}

StatusCode ForwardRingerDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  if (!collectInfo()) ATH_MSG_DEBUG("Unable to collect info. Please check input file");
  m_Tree->Fill();
  return StatusCode::SUCCESS;
}

StatusCode ForwardRingerDumperAlg::collectInfo(){
  clear();
  if(!collectEventInfo())   ATH_MSG_DEBUG("Event information cannot be collected!");
  if(!collectRingsInfo())   ATH_MSG_DEBUG("Ringer information cannot be collected!");
  if(!collectElectronInfo()) ATH_MSG_DEBUG("Offline Electron information cannot be collected!");
  return StatusCode::SUCCESS;
}

void ForwardRingerDumperAlg::clear(){
  m_pileup = -999;
  if(m_dumpCells){
    m_cells_eta->clear();
    m_cells_phi->clear();
    m_cells_et->clear();
    m_cells_sampling->clear();
    m_cells_ringNumber->clear();
    m_cells_seedEta->clear();
    m_cells_seedPhi->clear();
  }
  m_clusterEnergy->clear();
  m_clusterEta->clear();
  m_clusterPhi->clear();
  m_clustere237->clear();
  m_clustere277->clear();
  m_clusterfracs1->clear();
  m_clusterweta2->clear();
  m_clusterwstot->clear();
  m_clusterehad1->clear();
  m_offlineRings->clear();
  m_hasCalo->clear();
  m_el_calo_e->clear();
  m_el_calo_eta->clear();
  m_el_calo_phi->clear();
  m_el_calo_et->clear();
  m_el_calo_etaBE2->clear();
  el_energy->clear();
  el_et->clear();
  el_eta->clear();
  el_phi->clear();
  el_f1->clear();
  el_f3->clear();
  el_eratio->clear();
  el_weta1->clear();
  el_weta2->clear();
  el_fracs1->clear();
  el_wtots1->clear();
  el_deltae->clear();
  el_e277->clear();
  el_reta->clear();
  el_rphi->clear();
  el_rhad->clear();
  el_rhad1->clear();
  m_el_tight->clear();
  m_el_medium->clear();
  m_el_loose->clear();
  if(m_doMC){
    mc_et->clear();
    mc_eta->clear();
    mc_phi->clear();
    mc_origin->clear();
    mc_type->clear();
  }
}

StatusCode ForwardRingerDumperAlg::collectRingsInfo(){
 
    ATH_MSG_DEBUG("Rings Container Retrieved! That's good!");
    // const xAOD::RingSetContainer *el_fwd_ring_container = nullptr;
    // ATH_CHECK(evtStore()->retrieve(el_fwd_ring_container, "ForwardElectronRingSets"));
    // std::vector<float> single_ring;
    // for (const xAOD::RingSet_v1 *el_ring : *el_fwd_ring_container) {
    //     for (auto iter=el_ring->begin(); iter != el_ring->end(); iter++){
    //         single_ring.push_back(*iter);
    //     }
    //     if (single_ring.size() == 14){
    //         m_offlineRings->push_back(single_ring);
    //         single_ring.clear();
    //     }
    // }
    if(m_dumpCells){
      const xAOD::CaloRingsContainer *el_fwd_calorings_container = nullptr;
      ATH_CHECK(evtStore()->retrieve(el_fwd_calorings_container, "ForwardElectronCaloRings"));
      for (const xAOD::CaloRings_v1 *caloRings : *el_fwd_calorings_container){
        ATH_MSG_DEBUG("Looping over CaloRings Container");
        m_cells_eta->push_back(caloRings->auxdata<std::vector<float>>("cells_eta"));
        m_cells_phi->push_back(caloRings->auxdata<std::vector<float>>("cells_phi"));
        m_cells_et->push_back(caloRings->auxdata<std::vector<float>>("cells_et"));
        m_cells_sampling->push_back(caloRings->auxdata<std::vector<int>>("cells_sampling"));
        m_cells_ringNumber->push_back(caloRings->auxdata<std::vector<int>>("cells_ringNumber"));
        m_cells_seedEta->push_back(caloRings->auxdata<std::vector<float>>("cells_seedEta"));
        m_cells_seedPhi->push_back(caloRings->auxdata<std::vector<float>>("cells_seedPhi"));
      }
    }
  ATH_MSG_DEBUG("Offline Forward Standard Ringer Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

StatusCode ForwardRingerDumperAlg::collectEventInfo(){
  const xAOD::EventInfo* ei = nullptr;
  ATH_CHECK( evtStore()->retrieve(ei,"EventInfo"));
  ATH_MSG_DEBUG("EventInfo Container Retrieved! That's good!");
  m_pileup =  ei->actualInteractionsPerCrossing();
  return StatusCode::SUCCESS;
}

StatusCode ForwardRingerDumperAlg::collectElectronInfo(){
  const xAOD::ElectronContainer *offlineElectronsFwd = nullptr;
  ATH_CHECK(evtStore()->retrieve(offlineElectronsFwd, "ForwardElectrons"));
  ATH_MSG_DEBUG("Offline Electron Container Retrieved! That's good!");
  bool hasCalo(false);
  for (auto *electron : *offlineElectronsFwd){
    m_el_tight->push_back(getApproval (electron,"Tight"));
    m_el_medium->push_back(getApproval (electron,"Medium"));
    m_el_loose->push_back(getApproval (electron,"Loose"));
    el_energy->push_back ( electron->e() );
    el_et->push_back ( electron->pt());
    el_eta->push_back ( electron->eta() );
    el_phi->push_back ( electron->phi() );
    getCaloRings(electron, *m_offlineRings);
    if (!collectOfflineSS(electron)) ATH_MSG_DEBUG("Impossible to collect offline SS variables");
    if (m_doMC){
      const xAOD::TruthParticle* mc = xAOD::TruthHelpers::getTruthParticle(*electron);
      if(mc){
        mc_et->push_back(mc->pt());
        mc_eta->push_back(mc->eta());
        mc_phi->push_back(mc->phi());
        mc_origin->push_back(electron->auxdata<int>("truthOrigin"));
        mc_type->push_back(electron->auxdata<int>("truthType"));
      }
    }
    if(electron->caloCluster()){
      hasCalo  = true;
      m_el_calo_et->push_back(electron->caloCluster()->et());
      m_el_calo_eta->push_back(electron->caloCluster()->eta());
      m_el_calo_phi->push_back(electron->caloCluster()->phi());
      m_el_calo_etaBE2->push_back(electron->caloCluster()->etaBE(2));
      m_el_calo_e->push_back(electron->caloCluster()->e());
    }
    m_hasCalo->push_back(hasCalo);
  }
  ATH_MSG_DEBUG("Offline Electron Container Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

bool ForwardRingerDumperAlg::getApproval (const xAOD::Electron_v1 *eg , const std::string pidname){
    if (pidname == "Tight"){
        const asg::AcceptData accept=m_electronIsEMTool[0]->accept(eg);
        return static_cast<bool>(accept);
    }
    else if (pidname == "Medium"){
        const asg::AcceptData accept=m_electronIsEMTool[1]->accept(eg);
        return static_cast<bool>(accept);
    }
    else if (pidname == "Loose"){
        const asg::AcceptData accept=m_electronIsEMTool[2]->accept(eg);
        return static_cast<bool>(accept);
    }
    else ATH_MSG_DEBUG("No Pid tool, continue without PID");
    return false;
}

StatusCode  ForwardRingerDumperAlg::collectOfflineSS(const xAOD::Electron *electron){
  el_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::f1));
  el_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::f3));
  el_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::Eratio));
  el_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::weta1));
  el_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::weta2));
  el_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::fracs1));
  el_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::wtots1));
  el_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::e277));
  el_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::Reta));
  el_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::Rphi));
  el_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::DeltaE));
  el_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::Rhad));
  el_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::ShowerShapeType::Rhad1));
   
  return StatusCode::SUCCESS;
}

bool ForwardRingerDumperAlg::getCaloRings( const xAOD::Electron_v1 * el, std::vector<float> &ringsE ){

  
  if(!el) return false;
  ringsE.clear();

  auto m_ringsELReader = xAOD::getCaloRingsReader();
  // First, check if we can retrieve decoration: 
  const xAOD::CaloRingsLinks *caloRingsLinks(nullptr);
  try { 
    ATH_MSG_DEBUG("getCaloRingsReader->operator()(*el)");
    caloRingsLinks = &(m_ringsELReader(*el)); 
  } catch ( const std::exception &e) { 
    ATH_MSG_WARNING("Couldn't retrieve CaloRingsELVec. Reason: " << e.what()); 
    return false;
  } 

  if( caloRingsLinks ){
    if ( caloRingsLinks->empty() ){ 
      ATH_MSG_WARNING("Particle does not have CaloRings decoratorion.");
      return false;
    }

    const xAOD::CaloRings *clrings=nullptr;
    try {
      // For now, we are using only the first cluster 
      clrings = *(caloRingsLinks->at(0));
    } catch(const std::exception &e){
      ATH_MSG_WARNING("Couldn't retrieve CaloRings. Reason: " << e.what()); 
      return false;
    }
    // For now, we are using only the first cluster 
    if(clrings) {
      ATH_MSG_DEBUG("exportRingsTo...");
      clrings->exportRingsTo(ringsE);
    }else{
      ATH_MSG_WARNING("There is a problem when try to attack the rings vector using exportRigsTo() method.");
      return false;
    }

  }else{
    ATH_MSG_WARNING("CaloRingsLinks in a nullptr...");
    return false;
  }

  return true;
}

StatusCode ForwardRingerDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}
