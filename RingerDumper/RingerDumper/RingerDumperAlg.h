#ifndef RINGERDUMPER_RINGERDUMPERALG_H
#define RINGERDUMPER_RINGERDUMPERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
#include "xAODTrigRinger/TrigRingerRingsContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include <TTree.h>
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include <xAODCaloRings/versions/RingSet_v1.h>
#include <xAODCaloRings/RingSetContainer.h>
#include <xAODEgamma/versions/Electron_v1.h>
#include <xAODEgamma/ElectronContainer.h>
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include <xAODCaloRings/versions/CaloRings_v1.h>
#include <xAODCaloRings/CaloRingsContainer.h>
#include <iostream>
#include <vector>

using namespace std;

class RingerDumperAlg: public ::AthAlgorithm { 

 public: 
    RingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~RingerDumperAlg(); 

    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  execute();        //per event
    virtual StatusCode  finalize();       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    virtual StatusCode  collectInfo();
    virtual StatusCode  collectRingsInfo();
    virtual StatusCode  collectEventInfo();
    virtual StatusCode  collectOfflineInfo();
    virtual StatusCode  collectOfflineSS(const xAOD::Electron *electron);
    virtual StatusCode  buildQuarters(std::vector<float> &cells_eta, std::vector<float> &cell_phi, std::vector<float> &cells_et, std::vector<int> &cells_sampling);

  private: 
    TTree *m_Tree;

    ServiceHandle<ITHistSvc> m_ntsvc;
    Gaudi::Property<bool>                          m_doMC       {this, "doMC"     , false ,  "Dump MC information"};
    Gaudi::Property<bool>                          m_dumpCells  {this, "dumpCells", false ,  "Dump Cell information"};
    Gaudi::Property<bool>                          m_doQuarters {this, "doQuarters", false ,  "Do quarter rings"};
        
    float                                 m_pileup          = -999;
    
    std::vector < float >                 *m_clusterEnergy  = nullptr;
    std::vector < float >                 *m_clusterEta     = nullptr;
    std::vector < float >                 *m_clusterPhi     = nullptr;
    std::vector < float >                 *m_clustere237    = nullptr;
    std::vector < float >                 *m_clustere277    = nullptr;
    std::vector < float >                 *m_clusterfracs1  = nullptr;
    std::vector < float >                 *m_clusterweta2   = nullptr;
    std::vector < float >                 *m_clusterwstot   = nullptr;
    std::vector < float >                 *m_clusterehad1   = nullptr;
    std::vector < std::vector < float > > *m_rings          = nullptr;
    std::vector < std::vector < float > > *m_offlineRings   = nullptr;
    std::vector < std::vector < float > > *m_cells_eta      = nullptr;
    std::vector < std::vector < float > > *m_cells_phi      = nullptr;
    std::vector < std::vector < float > > *m_cells_et       = nullptr;
    std::vector < std::vector < int > >   *m_cells_sampling = nullptr;
    std::vector < std::vector < int > >   *m_cells_size     = nullptr;
    std::vector < std::vector < double > >*m_rings_sum     = nullptr;


    //truth
    std::vector < float >                 *m_TruthclusterEnergy  = nullptr;
    std::vector < float >                 *m_TruthclusterEta     = nullptr;
    std::vector < float >                 *m_TruthclusterPhi     = nullptr;
    std::vector < float >                 *m_Truthclustere237    = nullptr;
    std::vector < float >                 *m_Truthclustere277    = nullptr;
    std::vector < float >                 *m_Truthclusterfracs1  = nullptr;
    std::vector < float >                 *m_Truthclusterweta2   = nullptr;
    std::vector < float >                 *m_Truthclusterwstot   = nullptr;
    std::vector < float >                 *m_Truthclusterehad1   = nullptr;
    std::vector < std::vector < float > > *m_Truthrings          = nullptr;
    
    std::vector < std::vector < float > > *m_Truthcells_eta      = nullptr;
    std::vector < std::vector < float > > *m_Truthcells_phi      = nullptr;
    std::vector < std::vector < float > > *m_Truthcells_et       = nullptr;
    std::vector < std::vector < int > >   *m_Truthcells_sampling = nullptr;
    std::vector < std::vector < int > >   *m_Truthcells_size     = nullptr;
    std::vector < std::vector < double > >*m_Truthrings_sum     = nullptr;


    std::vector < float > *el_energy = nullptr;
    std::vector < float > *el_eta = nullptr;
    std::vector < float > *el_phi = nullptr;
    std::vector < float > *el_f1  = nullptr;
    std::vector < float > *el_f3  = nullptr;
    std::vector < float > *el_eratio = nullptr;
    std::vector < float > *el_weta1 = nullptr;  
    std::vector < float > *el_weta2 = nullptr;  
    std::vector < float > *el_fracs1 = nullptr; 
    std::vector < float > *el_wtots1 = nullptr; 
    std::vector < float > *el_e277 = nullptr; 
    std::vector < float > *el_reta = nullptr; 
    std::vector < float > *el_rphi = nullptr; 
    std::vector < float > *el_deltae = nullptr; 
    std::vector < float > *el_rhad = nullptr;  
    std::vector < float > *el_rhad1 = nullptr; 

    std::vector < float > *el_Truth_pt = nullptr;
    std::vector < float > *el_Truth_eta = nullptr;
    std::vector < float > *el_Truth_phi = nullptr;
    std::vector < float > *el_Truth_f1  = nullptr;
    std::vector < float > *el_Truth_f3  = nullptr;
    std::vector < float > *el_Truth_eratio = nullptr;
    std::vector < float > *el_Truth_weta1 = nullptr;  
    std::vector < float > *el_Truth_weta2 = nullptr;  
    std::vector < float > *el_Truth_fracs1 = nullptr; 
    std::vector < float > *el_Truth_wtots1 = nullptr; 
    std::vector < float > *el_Truth_e277 = nullptr; 
    std::vector < float > *el_Truth_reta = nullptr; 
    std::vector < float > *el_Truth_rphi = nullptr; 
    std::vector < float > *el_Truth_deltae = nullptr; 
    std::vector < float > *el_Truth_rhad = nullptr;  
    std::vector < float > *el_Truth_rhad1 = nullptr; 
    
    std::vector < float > *mc_eta = nullptr;
    std::vector < float > *mc_phi = nullptr;
    std::vector < float > *mc_et = nullptr;
    std::vector < int > *mc_origin = nullptr;
    std::vector < int > *mc_type = nullptr;


    std::vector < float > *mc_match_eta = nullptr;
    std::vector < float > *mc_match_phi = nullptr;
    std::vector < float > *mc_match_et = nullptr;
    std::vector < int > *mc_match_origin = nullptr;
    std::vector < int > *mc_match_type = nullptr;


    float dR(const float eta1, const float phi1, const float eta2, const float phi2) const
    {
      float deta = std::abs(eta1 - eta2);
      float dphi = std::abs(phi1 - phi2) < TMath::Pi() ? std::abs(phi1 - phi2) : 2*TMath::Pi() - std::abs(phi1 - phi2);
      return sqrt(deta*deta + dphi*dphi);
    };

}; 


#endif //> !RINGERDUMPER_RINGERDUMPERALG_H
