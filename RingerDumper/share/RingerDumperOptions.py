
###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
doFwd = False
doMC = True
dumpCells = True
doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
# from Gaudi.Configuration import *

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()
from RingerDumper.RingerDumperConf import ForwardRingerDumperAlg, RingerDumperAlg, PhotonRingerDumperAlg
if doPhoton:
    job+= PhotonRingerDumperAlg("RingerDumper")
    job.RingerDumper.dumpOfflineRings = False
elif doFwd:
    job += ForwardRingerDumperAlg( "RingerDumper" )   # 1 alg, named "HelloWorld"
    from ElectronPhotonSelectorTools.ElectronPhotonSelectorToolsConf import AsgForwardElectronIsEMSelector
    LooseElectronSelector             = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTLooseElectronSelector")
    MediumElectronSelector            = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTMediumElectronSelector")
    TightElectronSelector             = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTTightElectronSelector")
    
    LooseElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMLooseSelectorCutDefs.conf"
    MediumElectronSelector.ConfigFile = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMMediumSelectorCutDefs.conf"
    TightElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMTightSelectorCutDefs.conf"

    ToolSvc+=LooseElectronSelector
    ToolSvc+=MediumElectronSelector
    ToolSvc+=TightElectronSelector

    job.RingerDumper.ForwardElectronIsEMSelectors = [TightElectronSelector, MediumElectronSelector, LooseElectronSelector]
else:
    job += RingerDumperAlg( "RingerDumper" )   # 1 alg, named "HelloWorld"


job.RingerDumper.doMC = doMC
job.RingerDumper.dumpCells = dumpCells
job.RingerDumper.OutputLevel = INFO

from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

ServiceMgr.EventSelector.InputCollections = [ '/afs/cern.ch/user/e/eegidiop/RingerDumper/data/mc21_13p6TeV/AOD.28584754._000320.pool.root.1' ]
ServiceMgr += CfgMgr.THistSvc()
hsvc = ServiceMgr.THistSvc
hsvc.Output += [ "rec DATAFILE='ntuple_000_300.root' OPT='RECREATE'" ]
theApp.EvtMax = 250

