#!/usr/bin/env python
#
#  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#


import argparse
import os
from AthenaCommon.Logging import logging
mainLogger = logging.getLogger( 'RunRingerDumperOnGrid' )


parser = argparse.ArgumentParser(description = '', add_help = False)
parser = argparse.ArgumentParser()


#
# prun configuration
#

parser.add_argument('--inDS', action='store', dest='inDS', required = True, type=str, help = "Input dataset.")

parser.add_argument('--outDS', action='store', dest='outDS', required = True, type=str, help = "Output dataset.")

parser.add_argument('--nFilesPerJob', action='store', dest='nFilesPerJob', required = False, type=int, default=1,
                    help = "Number of files per job in grid mode.")

parser.add_argument('--job', action='store', dest='job', required = False, type=str, default='RunRingerDumper.py',
                    help = "Job name")

parser.add_argument('-a', '--asetup', action='store', dest='asetup', required = False, type=str, default='asetup Athena,master,latest,here',
                    help = "asetup release")

#
# Job Command
#

import sys,os
if len(sys.argv)==1:
  parser.print_help()
  sys.exit(1)


args = parser.parse_args()

execute = "{JOB} -i '%IN'".format(JOB=args.job)


command = """ prun --exec \
       "{COMMAND};" \
     --noBuild \
     --inDS={INDS} \
     --outDS={OUTDS} \
     --disableAutoRetry \
     --outputs="*.root" \
     --nFilesPerJob={N_FILES_PER_JOB} \
     --mergeOutput \
     --useAthenaPackage \
    """

command = command.format( 
                            INDS      = args.inDS,
                            OUTDS     = args.outDS,
                            COMMAND   = execute,
                            N_FILES_PER_JOB = args.nFilesPerJob,
                            )



print(command)
os.system(command)









