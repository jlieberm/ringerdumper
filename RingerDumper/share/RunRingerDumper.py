#!/usr/bin/env python
#
#  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#

if __name__=='__main__':

  import argparse
  import sys,os


  parser = argparse.ArgumentParser(description = '', add_help = False)
  parser = argparse.ArgumentParser()

  #
  # File configuration
  #

  parser.add_argument('-i','--inputFiles', action='store', dest='inputFiles', required = False,
                      help = "Comma-separated list of input files (alias for --filesInput)")

  parser.add_argument('-o','--outputFile', action='store', dest='outputFile', required = False,
                      default = 'TrigEgammaDumperOutput.root',
                      help = "Histogram output.")

  parser.add_argument('--nov','--numberOfEvents', action='store', dest='numberOfEvents', required = False, 
                      type=int, default=-1,
                      help = "The number of events to run.")


  if len(sys.argv)==1:
    parser.print_help()
    sys.exit(1)

  args = parser.parse_args()



  from PyUtils.Helpers import ROOT6Setup
  ROOT6Setup()
  # Setup the Run III behavior
  from AthenaCommon.Configurable import Configurable
  Configurable.configurableRun3Behavior = 1
  # Setup logs
  from AthenaCommon.Logging import log
  from AthenaCommon.Constants import INFO
  log.setLevel(INFO)


  # Set the Athena configuration flags
  from AthenaConfiguration.AllConfigFlags import ConfigFlags

  if args.inputFiles is not None:
    ConfigFlags.Input.Files = args.inputFiles.split(',')
  ConfigFlags.Input.isMC = True
  ConfigFlags.Output.HISTFileName = args.outputFile
  

  ConfigFlags.lock()


  # Initialize configuration object, add accumulator, merge, and run.
  from AthenaConfiguration.MainServicesConfig import MainServicesCfg 
  from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
  cfg = MainServicesCfg(ConfigFlags)
  cfg.merge(PoolReadCfg(ConfigFlags))

  
  # create the e/g monitoring tool and add into the component accumulator
#   from TrigEgammaMonitoring.TrigEgammaElectronDumper import TrigEgammaDumperConfig
  from RingerDumper.RingerDumperConfig import RingerDumperCfg
  RingerDumperAcc = RingerDumperCfg()
  cfg.merge(RingerDumperAcc)



  cfg.printConfig(withDetails=False) # set True for exhaustive info
  sc = cfg.run(args.numberOfEvents) #use cfg

  sys.exit(0 if sc.isSuccess() else 1)

