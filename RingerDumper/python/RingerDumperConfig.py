# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
from __future__ import print_function
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def RingerDumperCfg():
    result=ComponentAccumulator()
    
    RingerDumperAlg=CompFactory.RingerDumperAlg
    RingerDumper=RingerDumperAlg("RingerDumperExecution")
    
    RingerDumper.OutputLevel = 0

    # Set an int property
    # HelloWorld.MyInt = 42

    # HelloTool=CompFactory.HelloTool
    # ht=HelloTool( "HelloTool" )
    # HelloWorld.MyPrivateHelloTool = ht #HelloTool( "HelloTool" )
    # HelloWorld.MyPrivateHelloTool.MyMessage = "A Private Message!"

    # pt=HelloTool( "PublicHello")
    # pt.MyMessage="A public Message!"    

    # result.addPublicTool(pt)
    # print (pt)


    # HelloWorld.MyPublicHelloTool=pt


    #print HelloWorld

    result.addEventAlgo(RingerDumper)
    return result


if __name__=="__main__":
    from AthenaCommon.Configurable import Configurable
    Configurable.configurableRun3Behavior=1

    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    ConfigFlags.Exec.MaxEvents=10
    cfg=MainServicesCfg(ConfigFlags)
    cfg.merge(RingerDumperCfg())
    cfg.run()

    #f=open("HelloWorld.pkl","wb")
    #cfg.store(f)
    #f.close()

